<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PagesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Models\Books\Page');

        for($i = 1 ; $i <= 1000 ; $i++){
            \DB::table('pages')->insert([
                'book_id' => 1,
                'page'=> $i,
                'text' =>  $i,
                'language_id' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'Updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
