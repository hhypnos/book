<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $book = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'pages' =>  $this->when($this->relationLoaded('pages'), function () {
                return PageResource::collection($this->pages);
            }),
            'language' => $this->language,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at
        ];
        return $book;
    }
}
