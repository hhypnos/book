<?php

namespace App\Http\Resources;

use App\Traits\PageTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    use PageTrait;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $page = [
            'id' => $this->id,
            'book' => $this->when($this->relationLoaded('book'), function () {
                return new BookResource($this->book);
            }),
            'page' => $this->page,
            'text' => $this->text,
            'language' => new LanguageResource($this->language),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at
        ];
        return $page;
    }
}
