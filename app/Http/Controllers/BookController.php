<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Models\Books\Book;
use App\Traits\BookTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    use BookTrait;
    /**
     * @OA\Get(
     *   path="/books",
     *   tags={"Books"},
     *   summary="Fetch Books",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch Books",
     *   operationId="index",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Books"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Display all resources that have been added
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $validator = Validator::make(
            [
                'order_by' => request('order_by'),
                'data_per_page' => request('data_per_page')
            ],
            [
                'order_by' => ['nullable','in:asc,desc,ASC,DESC'],
                'data_per_page' => ['nullable','numeric']
            ]
        );
        if ($validator->fails())
        {
            return response(['success' =>false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]],400);
        }
        $orderBy = is_null(request('order_by')) ? 'ASC' : request('order_by');
        $dataPerPage = request('data_per_page') ? request('data_per_page') : 20;
        $books = Book::orderBy('id',$orderBy)->paginate($dataPerPage);
        return ['success' =>true, 'code' => 'success','status' => 200, 'result' => [
            'data' => BookResource::collection($books),
            'pagination' => collect($books->toArray())->except('data'),
        ]];
    }

    /**
     * @OA\Get(
     *   path="/books/{id}",
     *   tags={"Books"},
     *   summary="Fetch Book",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch Book",
     *   operationId="show",
     *     @OA\Parameter(
     *         description="Book id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Book"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Display specific resource that have been added
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book){
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new BookResource($book)]]);
    }

    /**
     * @OA\Post(
     *   path="/books",
     *   tags={"Books"},
     *   summary="Store Book",
     *   security={{"bearerAuth":{}}},
     *   description="Store new Book",
     *   operationId="store",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     example="Book 1",
     *                     description="Book Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     example="book description",
     *                     description="book description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="lang_id",
     *                     example="1",
     *                     description="Default language is 1",
     *                     type="integer",
     *                 )
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="successful operation",
     *   @OA\JsonContent(
     *                 @OA\Property(
     *                     example="{'success':true,'code':'success','status':200,'result':{'data':{'id':5,'first_name':'ggg','last_name':'gg','date_of_birth':'1998-11-17','email':'g2@g.com','gender':1,'created_at':'2021-01-04 13:23:30','updated_at':'2021-01-04 13:23:30'}}}",
     *                     description="Example Response",
     *                     type="object"
     *                 )
     * )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid username/password supplied"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make(
            [
                'name' => $request->name,
                'description' => $request->description,
                'lang_id' => $request->lang_id,
            ],
            [
                'name' => 'required',
                'description' => 'required',
                'lang_id' => 'required|exists:languages,id,deleted_at,NULL',
            ]
        );
        if ($validator->fails())
        {
            return response(['success' =>false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]],400);
        }
        $book = new Book;
        $book->name = $request->name;
        $book->description = $request->description;
        $book->lang_id = $request->lang_id;
        $book->save();
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new BookResource($book)]]);
    }

    /**
     * @OA\Put(
     *   path="/books/{id}",
     *   tags={"Books"},
     *   summary="Store Book",
     *   security={{"bearerAuth":{}}},
     *   description="Store new Book",
     *   operationId="update",
     *     @OA\Parameter(
     *         description="id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     example="Book 1",
     *                     description="Book Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     example="book description",
     *                     description="book description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="lang_id",
     *                     example="1",
     *                     description="Default language is 1",
     *                     type="integer",
     *                 )
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="successful operation",
     *   @OA\JsonContent(
     *                 @OA\Property(
     *                     example="{'success':true,'code':'success','status':200,'result':{'data':{'id':5,'first_name':'ggg','last_name':'gg','date_of_birth':'1998-11-17','email':'g2@g.com','gender':1,'created_at':'2021-01-04 13:23:30','updated_at':'2021-01-04 13:23:30'}}}",
     *                     description="Example Response",
     *                     type="object"
     *                 )
     * )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid username/password supplied"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book){
        $validator = Validator::make(
            [
                'name' => $request->name,
                'description' => $request->description,
                'lang_id' => $request->lang_id,
            ],
            [
                'name' => 'required',
                'description' => 'required',
                'lang_id' => 'required|exists:languages,id,deleted_at,NULL',
            ]
        );
        if ($validator->fails())
        {
            return response(['success' =>false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]],400);
        }
        $book->name = $request->name;
        $book->description = $request->description;
        $book->lang_id = $request->lang_id;
        $book->save();
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new BookResource($book)]]);
    }

    /**
     * @OA\Delete(
     *   path="/books/{id}",
     *   tags={"Books"},
     *   summary="Delete Books",
     *   security={{"bearerAuth":{}}},
     *   description="Delete Book",
     *   operationId="destroy",
     *     @OA\Parameter(
     *         description="Book id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Delete Book"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return response()->json(['success' =>true, 'code' => 'success','status' => 204, 'result'=>['data'=>null] ]);
    }
}
