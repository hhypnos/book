<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\LanguageResource;
use App\Models\Languages\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LanguageController extends Controller
{
    /**
     * @OA\Get(
     *   path="/languages",
     *   tags={"Languages"},
     *   summary="Fetch Languages",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch Languages",
     *   operationId="index",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Languages"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Display all resources that have been added
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $validator = Validator::make(
            [
                'order_by' => request('order_by'),
                'data_per_page' => request('data_per_page')
            ],
            [
                'order_by' => ['nullable', 'in:asc,desc,ASC,DESC'],
                'data_per_page' => ['nullable', 'numeric']
            ]
        );
        if ($validator->fails()) {
            return response(['success' => false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]], 400);
        }
        $orderBy = is_null(request('order_by')) ? 'ASC' : request('order_by');
        $dataPerPage = request('data_per_page') ? request('data_per_page') : 20;
        $languages = Language::orderBy('id', $orderBy)->paginate($dataPerPage);
        return ['success' => true, 'code' => 'success', 'status' => 200, 'result' => [
            'data' => LanguageResource::collection($languages),
            'pagination' => collect($languages->toArray())->except('data'),
        ]];
    }

    /**
     * @OA\Get(
     *   path="/languages/{id}",
     *   tags={"Languages"},
     *   summary="Fetch Language",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch Language",
     *   operationId="show",
     *     @OA\Parameter(
     *         description="Language id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Language"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Display specific resource that have been added
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        return response()->json(['success' => true, 'code' => 'success', 'status' => 200, 'result' => ['data' => new LanguageResource($language)]]);
    }

    /**
     * @OA\Post(
     *   path="/languages",
     *   tags={"Languages"},
     *   summary="Store Language",
     *   security={{"bearerAuth":{}}},
     *   description="Store new Language",
     *   operationId="store",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     example="name",
     *                     description="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="acronym",
     *                     example="acronym",
     *                     description="acronym",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="order_id",
     *                     example="order_id",
     *                     description="order_id",
     *                     type="integer",
     *                 ),
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="successful operation",
     *   @OA\JsonContent(
     *                 @OA\Property(
     *                     example="{'success':true,'code':'success','status':200,'result':{'data':{'id':5,'first_name':'ggg','last_name':'gg','date_of_birth':'1998-11-17','email':'g2@g.com','gender':1,'created_at':'2021-01-04 13:23:30','updated_at':'2021-01-04 13:23:30'}}}",
     *                     description="Example Response",
     *                     type="object"
     *                 )
     * )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid username/password supplied"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            [
                'name' => $request->name,
                'acronym' => $request->acronym,
            ],
            [
                'name' => 'required',
                'acronym' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response(['success' => false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]], 400);
        }
        $language = new Language;
        $language->name = $request->name;
        $language->acronym = $request->acronym;
        $language->order_id = $request->order_id;
        $language->save();
        return response()->json(['success' => true, 'code' => 'success', 'status' => 200, 'result' => ['data' => new LanguageResource($language)]]);
    }

    /**
     * @OA\Put(
     *   path="/languages/{id}",
     *   tags={"Languages"},
     *   summary="Update Languages",
     *   security={{"bearerAuth":{}}},
     *   description="Update new Languages",
     *   operationId="update",
     *     @OA\Parameter(
     *         description="id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     example="name",
     *                     description="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="acronym",
     *                     example="acronym",
     *                     description="acronym",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="order_id",
     *                     example="order_id",
     *                     description="order_id",
     *                     type="integer",
     *                 ),
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="successful operation",
     *   @OA\JsonContent(
     *                 @OA\Property(
     *                     example="{'success':true,'code':'success','status':200,'result':{'data':{'id':5,'first_name':'ggg','last_name':'gg','date_of_birth':'1998-11-17','email':'g2@g.com','gender':1,'created_at':'2021-01-04 13:23:30','updated_at':'2021-01-04 13:23:30'}}}",
     *                     description="Example Response",
     *                     type="object"
     *                 )
     * )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid username/password supplied"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        $validator = Validator::make(
            [
                'name' => $request->name,
                'acronym' => $request->acronym,
            ],
            [
                'name' => 'required',
                'acronym' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response(['success' => false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]], 400);
        }
        $language->name = $request->name;
        $language->acronym = $request->acronym;
        $language->order_id = $request->order_id;
        $language->save();
        return response()->json(['success' => true, 'code' => 'success', 'status' => 200, 'result' => ['data' => new LanguageResource($language)]]);
    }

    /**
     * @OA\Delete(
     *   path="/languages/{id}",
     *   tags={"Languages"},
     *   summary="Delete Language",
     *   security={{"bearerAuth":{}}},
     *   description="Delete Language",
     *   operationId="destroy",
     *     @OA\Parameter(
     *         description="Language id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Delete Language"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        $language->delete();
        return response()->json(['success' => true, 'code' => 'success', 'status' => 204, 'result' => ['data' => null]]);
    }
}
