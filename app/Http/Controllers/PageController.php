<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\PageResource;
use App\Models\Pages\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    /**
     * @OA\Get(
     *   path="/pages",
     *   tags={"Pages"},
     *   summary="Fetch Pages",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch Pages",
     *   operationId="index",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Pages"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Display all resources that have been added
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $validator = Validator::make(
            [
                'order_by' => request('order_by'),
                'data_per_page' => request('data_per_page')
            ],
            [
                'order_by' => ['nullable','in:asc,desc,ASC,DESC'],
                'data_per_page' => ['nullable','numeric']
            ]
        );
        if ($validator->fails())
        {
            return response(['success' =>false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]],400);
        }
        $orderBy = is_null(request('order_by')) ? 'ASC' : request('order_by');
        $dataPerPage = request('data_per_page') ? request('data_per_page') : 20;
        $pages = Page::orderBy('id',$orderBy)->paginate($dataPerPage);
        return ['success' =>true, 'code' => 'success','status' => 200, 'result' => [
            'data' => PageResource::collection($pages),
            'pagination' => collect($pages->toArray())->except('data'),
        ]];
    }

    /**
     * @OA\Get(
     *   path="/pages/{id}",
     *   tags={"Pages"},
     *   summary="Fetch Page",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch Page",
     *   operationId="show",
     *     @OA\Parameter(
     *         description="Page id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Page"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Display specific resource that have been added
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page){
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new PageResource($page)]]);
    }

    /**
     * @OA\Post(
     *   path="/pages",
     *   tags={"Pages"},
     *   summary="Store Page",
     *   security={{"bearerAuth":{}}},
     *   description="Store new Page",
     *   operationId="store",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     example="Page 1",
     *                     description="Page Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     example="book description",
     *                     description="book description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="lang_id",
     *                     example="1",
     *                     description="Default language is 1",
     *                     type="integer",
     *                 )
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="successful operation",
     *   @OA\JsonContent(
     *                 @OA\Property(
     *                     example="{'success':true,'code':'success','status':200,'result':{'data':{'id':5,'first_name':'ggg','last_name':'gg','date_of_birth':'1998-11-17','email':'g2@g.com','gender':1,'created_at':'2021-01-04 13:23:30','updated_at':'2021-01-04 13:23:30'}}}",
     *                     description="Example Response",
     *                     type="object"
     *                 )
     * )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid username/password supplied"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make(
            [
                'name' => $request->name,
                'description' => $request->description,
                'lang_id' => $request->lang_id,
            ],
            [
                'name' => 'required',
                'description' => 'required',
                'lang_id' => 'required|exists:languages,id,deleted_at,NULL',
            ]
        );
        if ($validator->fails())
        {
            return response(['success' =>false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]],400);
        }
        $page = new Page;
        $page->name = $request->name;
        $page->description = $request->description;
        $page->lang_id = $request->lang_id;
        $page->save();
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new PageResource($page)]]);
    }

    /**
     * @OA\Put(
     *   path="/pages/{id}",
     *   tags={"Pages"},
     *   summary="Store Page",
     *   security={{"bearerAuth":{}}},
     *   description="Store new Page",
     *   operationId="update",
     *     @OA\Parameter(
     *         description="id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     example="Page 1",
     *                     description="Page Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     example="book description",
     *                     description="book description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="lang_id",
     *                     example="1",
     *                     description="Default language is 1",
     *                     type="integer",
     *                 )
     *             )
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="successful operation",
     *   @OA\JsonContent(
     *                 @OA\Property(
     *                     example="{'success':true,'code':'success','status':200,'result':{'data':{'id':5,'first_name':'ggg','last_name':'gg','date_of_birth':'1998-11-17','email':'g2@g.com','gender':1,'created_at':'2021-01-04 13:23:30','updated_at':'2021-01-04 13:23:30'}}}",
     *                     description="Example Response",
     *                     type="object"
     *                 )
     * )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Something went wrong"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid username/password supplied"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page){
        $validator = Validator::make(
            [
                'name' => $request->name,
                'description' => $request->description,
                'lang_id' => $request->lang_id,
            ],
            [
                'name' => 'required',
                'description' => 'required',
                'lang_id' => 'required|exists:languages,id,deleted_at,NULL',
            ]
        );
        if ($validator->fails())
        {
            return response(['success' =>false, 'code' => 'error', 'status' => 400, 'result' => ['message' => $validator->messages()]],400);
        }
        $page->name = $request->name;
        $page->description = $request->description;
        $page->lang_id = $request->lang_id;
        $page->save();
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new PageResource($page)]]);
    }

    /**
     * @OA\Delete(
     *   path="/pages/{id}",
     *   tags={"Pages"},
     *   summary="Delete Pages",
     *   security={{"bearerAuth":{}}},
     *   description="Delete Page",
     *   operationId="destroy",
     *     @OA\Parameter(
     *         description="Page id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Delete Page"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        return response()->json(['success' =>true, 'code' => 'success','status' => 204, 'result'=>['data'=>null] ]);
    }
}
