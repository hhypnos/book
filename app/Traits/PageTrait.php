<?php

namespace App\Traits;
use App\Http\Resources\PageResource;
use App\Http\Resources\PersonResource;
use App\Models\Books\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait PageTrait {
    /**
     * @OA\Get(
     *   path="/pages/entities/{id}",
     *   tags={"Pages"},
     *   summary="Fetch Pages with entities",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch entities",
     *   operationId="page_entities",
     *     @OA\Parameter(
     *         description="Page id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="relation name",
     *         in="query",
     *         name="relations[]",
     *         required=true,
     *         @OA\Schema(
     *              type="array",
     *              @OA\Items( type="enum", enum={"book"} ),
     *              example={1,2}
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Person"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    public function page_entities(Request $request, Page $page) {
        $relations = [];
        foreach($request->relations as $key => $relation){
            if($relation == 'book'){
                $relations[]='book';
            }
        }
        return response()->json(['success' =>true, 'code' => 'success','status' => 200, 'result'=>['data'=> new PageResource($page->load($relations))]]);
    }
}
