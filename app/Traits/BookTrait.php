<?php

namespace App\Traits;
use App\Http\Resources\BookResource;
use App\Http\Resources\PageResource;
use App\Models\Books\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait BookTrait {
    /**
     * @OA\Get(
     *   path="/books/entities/{id}",
     *   tags={"Books"},
     *   summary="Fetch Books with entities",
     *   security={{"bearerAuth":{}}},
     *   description="Fetch entities",
     *   operationId="book_entities",
     *     @OA\Parameter(
     *         description="Book id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="relation name",
     *         in="query",
     *         name="relations[]",
     *         required=true,
     *         @OA\Schema(
     *              type="array",
     *              @OA\Items( type="enum", enum={"pages"} ),
     *              example={1,2}
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *   @OA\Response(
     *   response=200,
     *   description="Fetching Person"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Not authorized"
     *     )
     * )
     */
    public function book_entities(Request $request, Book $book) {
        $relations = [];
        foreach($request->relations as $key => $relation){
            if($relation == 'pages'){
                $relations[]='pages';
            }
        }
        $dataPerPage = request('data_per_page') ? request('data_per_page') : 2;
        return ['success' =>true, 'code' => 'success','status' => 200, 'result' => [
            'data' => PageResource::collection($book->pages()->paginate($dataPerPage)),
            'pagination' => collect($book->pages()->paginate($dataPerPage)->toArray())->except('data'),
        ]];
    }
}
