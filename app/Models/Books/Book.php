<?php

namespace App\Models\Books;

use App\Models\Languages\Language;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
    ];

    public function language(){
        return $this->belongsTo(Language::class);
    }

    public function pages(){
        return $this->hasMany(Page::class)->orderBy('page','ASC');
    }
}
