<?php

namespace App\Models\Books;

use App\Models\Languages\Language;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use HasFactory, SoftDeletes;

    public function language(){
        return $this->belongsTo(Language::class);
    }

    public function book(){
        return $this->belongsTo(Book::class);
    }
}
