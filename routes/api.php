<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\BookController;
use \App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| BOOKS
|--------------------------------------------------------------------------
|
|
*/
Route::get('/books/entities/{book}', [BookController::class, 'book_entities']);
Route::apiResource('/books', BookController::class);

/*
|--------------------------------------------------------------------------
| PAGES
|--------------------------------------------------------------------------
|
|
*/
Route::get('/pages/entities/{page}', [PageController::class, 'page_entities']);
Route::apiResource('/pages', PageController::class);
